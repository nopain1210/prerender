module.exports = {
	pageLoaded: (req, res, next) => {
		if (!req.prerender.content || req.prerender.renderType != 'html') {
			return next();
		}

		// Remove img, style tags, loading element, some redundant attributes
		req.prerender.content = req.prerender.content.replace(/<svg.*?>.*?<\/svg>|<svg.*?\/>|<img .*?>|data-react-helmet="true"|<div *class=" *loading__container *">.*?(<\/div>)+|(class|style|data-*.*?) *= *".*?"|<style>.*?<\/style>|<link +href=".+?.css" +rel="stylesheet" *\/?>/g,'');

		// Remove script tag
		var matches = req.prerender.content.toString().match(/<script(?:.*?)>(?:[\S\s]*?)<\/script>/gi);
		for (let i = 0; matches && i < matches.length; i++) {
			if (matches[i].indexOf('application/ld+json') === -1) {
				req.prerender.content = req.prerender.content.toString().replace(matches[i], '');
			}
		}

		//<link rel="import" src=""> tags can contain script tags. Since they are already rendered, let's remove them
		matches = req.prerender.content.toString().match(/<link[^>]+?rel="import"[^>]*?>/i);
		for (let i = 0; matches && i < matches.length; i++) {
			req.prerender.content = req.prerender.content.toString().replace(matches[i], '');
		}

		next();
	}
};