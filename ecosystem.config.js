module.exports = {
  apps : [{
    name: 'Prerender',
    script: './server.js',
    cwd: __dirname,
    autorestart: true,
    exec_mode: 'cluster',
    env: {
      NODE_ENV: "production",
      PAGE_LOAD_TIMEOUT: 3000,
      PAGE_DONE_CHECK_INTERVAL: 50,
      PORT: 3212
    }
  }],

  deploy : {
    production : {
      user : 'trancong',
      host : '45.76.163.114',
      ref  : 'origin/master',
      repo : 'https://gitlab.com/nopain1210/prerender.git',
      ssh_options: ['PasswordAuthentication=no', 'StrictHostKeyChecking=no'],
      path : '/web/bds/prerender',
      'post-setup': 'ls -la',
      'post-deploy' : 'npm install && pm2 startOrRestart ecosystem.config.js',
    }
  }
};

